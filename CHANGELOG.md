# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.1] - 2022-06-27
### Fixed
- Fix bug: `display` was not recognised for indentation
- Fix bug: annotations are no longer style checked

## [1.0.0] - 2022-06-26
### Added
- 1.0.0 release of FOLint, big thanks to [Lars Vermeulen](https://github.com/larsver) who started this project during his master's thesis.
